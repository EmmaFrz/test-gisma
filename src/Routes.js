import React from "react";
import {
  Routes,
  Route,
  BrowserRouter
} from "react-router-dom";

import Intro from './Intro';
import App from './App';

const AppRouter = () => {
    return(
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Intro />} />
                <Route path='list' element={<App />} />
            </Routes>
        </BrowserRouter>
    );
}

export default AppRouter;