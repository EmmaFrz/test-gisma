import { useNavigate  } from "react-router-dom";
import './assets/css/App.css'
import Image from './assets/img/happy.png'

const Intro = () => {
    let navigate = useNavigate();
    return(
        <center className='App'>
            <h3>bienvenidos a evaluacion tecnica de GISMA</h3>
            <img src={Image} alt='happy-mug' className="mug"/>
            <br/>
            <button className='introButton' onClick={() => navigate('/list', {repalce:'true'})}>Comencemos</button>
            <h4>Hecho por Emmanuel Franquiz, Abril, 2021</h4>
        </center>
    )
}

export default Intro;