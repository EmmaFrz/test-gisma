import { useState } from 'react';
import Form from './Components/Form';
import List from './Components/List';
import './assets/css/App.css';

function App() {
  const [todo, setTodo] = useState([]);
  const [input, setInput] = useState({add:''})

  const addNewtask = (task) => {
    const { add } = input;
    setTodo(prevState => {
      return [...prevState, add]
    });
    setInput({add:''})
  }

  const handleinput = (event) => {
    setInput(prevState => {
      return{
        ...prevState,
        [event.target.name]: event.target.value
      }
    });
  }

  const deleteTask = (task) => {
    const filter = todo.filter( value => {
      return task !== value;
    })
    setTodo(filter);
  }

  return (
    <center className="App">
      <Form input={input} handleinput={handleinput} addNewtask={addNewtask} />
      <List todo={todo} deleteTask={deleteTask}/>
    </center>
  );
}

export default App;
