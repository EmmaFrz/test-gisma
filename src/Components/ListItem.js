import '../assets/css/App.css';

const List = ({todo, deleteTask}) => {
    return(
        <>
            <ul className='list'>
                {todo.map(task => {
                    return(
                        <li className='listItem'>{task}<button className='deleteButton' onClick={() => deleteTask(task)}>Eliminar</button></li>
                    )
                })}
            </ul>
        </>
    )
}

export default List;