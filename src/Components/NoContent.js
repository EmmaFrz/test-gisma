import Image from '../assets/img/sad.png';

const Content = () => {
    return(
        <>        
            <img src={Image} alt='sadface' />
            <h4>No hay nada en la lista :(</h4>
        </>
    )
}

export default Content;