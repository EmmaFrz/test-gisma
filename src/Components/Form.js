import '../assets/css/App.css';

const Form = ({input, handleinput, addNewtask}) => {
    const { add } = input;
    return(
        <>
            <input placeholder="Ingresa una tarea" value={add} onChange={handleinput} name='add' className='input'/>
            <button onClick={addNewtask} className='addButton'>Agregar</button>
        </>
    )
}

export default Form;