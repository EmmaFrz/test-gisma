import ListItems from './ListItem';
import Content from './NoContent';
const List = ({todo, deleteTask}) => {
    return(
        <>
            <h2>Lista de usuarios</h2>
            {todo.length === 0 ? <Content /> : <ListItems todo={todo} deleteTask={deleteTask}/>}
        </>
    )
}

export default List;