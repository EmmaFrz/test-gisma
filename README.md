# Informacion Inicial

Esto es un pequeño test realizado para revisar el conocimiento y uso de React

## Instalar
Luego de descargar el proyecto usar el siguiente comando

```bash
npm install
```
Y luego de realizar dicha accion usar el siguiente comando 

```bash
npm run start
```

## Conclusiones 
Esta evaluacion cuenta con algunas falencias en cuanto a estilos, sin embargo funciona correctamente a nivel de manejo de estado y separacion de componentes.


## Arquitectura
Esta prueba tiene una arquitectura de separacion de componentes presentacionales, que reciben props para mostrar o ejecutar funciones segun sea el caso, alojados en un container principal que es el manejador de los estados y de las funciones, donde tambien se colocan los componentes presentacionales para que rendericen y reciban las funciones y estados que necesitan para un correcto funcionamiento


## Licencia
[MIT](https://choosealicense.com/licenses/mit/)